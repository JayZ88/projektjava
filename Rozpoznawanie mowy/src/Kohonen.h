
#ifndef _KOHONEN_
#define _KOHONEN_

#include <windows.h>
#include "globals.h"

const int NUMBER_INPUT = DOWNSAMPLE_HEIGHT * DOWNSAMPLE_WIDTH;
const int NUMBER_OUTPUT = NUMBER_OF_LETTERS;
const double LEARN_RATE = 0.3;
const double QUIT_ERROR = 0.1;
const int RETRIES = 10000;
const double REDUCTION = 0.99;

const int NB_TRAINING_SET = NUMBER_OUTPUT;


class TrainingSet 
{

public:

   TrainingSet();
   ~TrainingSet();
   void SetInput(int set, int index, double value);
  
   double input[NB_TRAINING_SET][NUMBER_INPUT];
   int ntrain;

private:
   int nin;
};


class Kohonen 
{
public:

   Kohonen();
   ~Kohonen();
   int Winner(double *input, double *normfac);
   BOOL Learn(TrainingSet *tptr);
   int *GetMapNeurons();
   
   double output[NUMBER_OUTPUT];
   double outputWeights[NUMBER_OUTPUT][NUMBER_INPUT];
   double s_neterr;
   
private:
   double VectorLength (int n, double *vec );
   void NormalizeInput(double *input, double *normfac);
   double dotProduct (int n, double *vec1, double *vec2);
   void Initialize();
   void ClearWeights();
   void RandomizeWeights();
   void NormalizeWeight(double *w);
   void EvaluateErrors(TrainingSet *tptr, double rate, int *won, double *bigerr, double **correc);
   void CopyWeights(Kohonen *dest, Kohonen *source);
   void ForceWin(TrainingSet *tptr, int *won);
   void AdjustWeights(double rate, int *won, double *bigcorr, double **correc);
   void CreateMap(TrainingSet *tptr);

   int mapNeuron[NUMBER_OF_LETTERS];
};

#endif
