/*-------------------------------------------------------
   KOHONEN.CPP -- Implementacja sieci neuronowej Kohonena

   Miesi�cznik "Software 2.0" - luty 2003.

   Autor: Krzysztof Osipowicz
   www:    www.krzyso.prv.pl
   e-mail: krzyso@poczta.onet.pl
  ------------------------------------------------------*/

#include <math.h>
#include <time.h>
#include "Kohonen.h"


TrainingSet::TrainingSet()
{
   ntrain = NB_TRAINING_SET;
   nin = NUMBER_INPUT;
}


TrainingSet::~TrainingSet ()
{
}


void TrainingSet::SetInput(int set, int index, double value)
{
   if ( (set<0) || (set>=ntrain) )
      return;
   if ( (index<0) || (index>=nin) )
      return;    
   input[set][index] = value;
}



Kohonen::Kohonen()
{
   const int NUMBER_INPUT = DOWNSAMPLE_HEIGHT * DOWNSAMPLE_WIDTH;
   const int NUMBER_OUTPUT = NUMBER_OF_LETTERS;
}

Kohonen::~Kohonen()
{
}


double Kohonen::VectorLength (int n, double *vec )
{
   double sum = 0.0;
   
   for (int i=0;i<n;i++ )
      sum += vec[i] * vec[i];
   return sum;
}

void Kohonen::NormalizeInput(double *input, double *normfac)
{
   double length;

   length = VectorLength(NUMBER_INPUT, input);
   if(length < 1.e-30)
      length = 1.e-30 ;

   *normfac = 1.0 / sqrt(length);
}


double Kohonen::dotProduct (int n, double *vec1, double *vec2)
{
   int k, m ;
   double sum;

   sum = 0.0;
   k = n / 4;
   m = n % 4;

   while (k--) 
   {
      sum += *vec1 * *vec2 ;
      sum += *(vec1+1) * *(vec2+1) ;
      sum += *(vec1+2) * *(vec2+2) ;
      sum += *(vec1+3) * *(vec2+3) ;
      vec1 += 4 ;
      vec2 += 4 ;
   }

   while (m--)
      sum += *vec1++ * *vec2++ ;

   return sum ;
} // dotProduct

void Kohonen::ClearWeights()
{
   s_neterr = 1.0;
   for (int y=0;y<NUMBER_OUTPUT;y++)
      for (int x=0;x<NUMBER_INPUT;x++)
         outputWeights[y][x] = 0.0;
}

void Kohonen::RandomizeWeights()
{
   double r;

   srand( (unsigned)time( NULL ) );
   int temp = (int)(3.464101615 / (2.0 * rand() )) ;

   for ( int y=0;y<NUMBER_OUTPUT;y++ ) 
   {
      for ( int x=0;x<NUMBER_INPUT;x++ ) 
      {
         r = (double)rand() + (double)rand() - (double)rand() - (double)rand() ;
         outputWeights[y][x] = r;
      }
   }
}

void Kohonen::NormalizeWeight(double *w)
{
   int i ;
   double len ;

   len = VectorLength(NUMBER_INPUT, w );
   if (len < 1.e-30)           
      len = 1.e-30 ;

   len = 1.0 / sqrt(len);
   for (i=0; i<NUMBER_INPUT; i++)
      w[i] *= len;
}

void Kohonen::Initialize()
{
   int i ;
   double *optr ;

   ClearWeights();
   RandomizeWeights();
   for (i=0 ; i<NUMBER_OUTPUT; i++) 
   {
      optr = outputWeights[i];
      NormalizeWeight(optr);
   }
} // Initialize


void Kohonen::EvaluateErrors(TrainingSet *tptr, double rate, int *won, double *bigerr, double **correc)
{
   int i, best, size, nwts, tset ;
   double *dptr, normfac, *cptr, *wptr, length, diff ;

   nwts = NUMBER_OUTPUT * NUMBER_INPUT;
   size = NUMBER_INPUT;

/*
   Zero cumulative corrections and winner counts
*/

   i = nwts ;
   for ( int y=0; y<NUMBER_OUTPUT; y++ ) 
   {
      for ( int x=0; x<NUMBER_INPUT; x++ ) 
      {
         correc[y][x]=0.0;
      }
   }


   memset (won, 0, NUMBER_OUTPUT * sizeof(int) ) ;
   *bigerr = 0.0 ;  // Length of biggest error vector

/*
   Cumulate the correction vector 'correc' across the epoch
*/

   for (tset=0 ; tset<tptr->ntrain ; tset++) 
   {
      dptr = tptr->input[tset] ; // Point to this case
      best = Winner(dptr, &normfac) ; // Winning neuron
      ++won[best];                   // Record this win
      wptr = outputWeights[best] ; // Winner's weights here
      cptr = correc[best];    // Corrections summed here
      length = 0.0 ;                  // Length of error vector

      for (i=0 ; i<NUMBER_INPUT ; i++) 
      {
         diff = dptr[i] * normfac - wptr[i]; // Input minus weight
         length += diff * diff; // Cumulate length of error
         cptr[i] += diff;    // just uses differences
      }                       // Loop does actual inputs
      
      if (length > *bigerr)      // Keep track of largest error
         *bigerr = length;

   }
   *bigerr = sqrt(*bigerr);
} // EvaluateErrors


void Kohonen::CopyWeights(Kohonen *dest, Kohonen *source)
{
   int n ;

   dest->s_neterr = source->s_neterr;
   n = NUMBER_OUTPUT * NUMBER_INPUT;
   memcpy(dest->outputWeights, source->outputWeights, n * sizeof(double) ) ;
} // CopyWeights


void Kohonen::ForceWin(TrainingSet *tptr, int *won)
{
   int i, tset, best, size, which ;
   double *dptr, dist, *optr, normfac;

   
   size = NUMBER_INPUT;

   dist = 1.e30 ;
   for (tset=0 ; tset<tptr->ntrain ; tset++) 
   {
      dptr = tptr->input[tset]; // Point to this case
      best = Winner (dptr, &normfac) ; // Winning neuron
      if (output[best] < dist) 
      {  // Far indicated by low activation
         dist = output[best];    // Maintain record
         which = tset;        // and which case did it
      }
   }

   dptr = tptr->input[which];
   best = Winner(dptr, &normfac);

   dist = -1.e30 ;
   i = NUMBER_OUTPUT;
   while (i--) 
   {           // Try all neurons
      if (won[i] != 0)          // If this one won then skip it
         continue ;        // We want a non-winner
      if (output[i] > dist) 
      { // High activation means similar
         dist = output[i];   // Keep track of best
         which = i;       // and its subscript
      }
   }

   optr = outputWeights[which];        // Non-winner's weights
   memcpy(optr, dptr, NUMBER_INPUT*sizeof(double) ) ; // become case
   NormalizeWeight(optr);
} // ForceWin

void Kohonen::AdjustWeights(double rate, int *won, double *bigcorr, double **correc)
{
   int i, j ;
   double corr, *cptr, *wptr, length, f;

   
   *bigcorr = 0.0;
   for (i=0; i<NUMBER_OUTPUT; i++) 
   {
      if (won[i]==0)
         continue;

      wptr = outputWeights[i];
      cptr = correc[i];

      f = (1.0 / (double) won[i])*rate;

      length = 0.0 ;

      for (j=0; j<NUMBER_INPUT; j++) 
      {
         corr = f * cptr[j];
         wptr[j] += corr;
         length += corr * corr;
      }

      if (length > *bigcorr)
         *bigcorr = length ;
   }

   *bigcorr = sqrt ( *bigcorr ) / rate ;

} // AdjustWeights

void Kohonen::CreateMap(TrainingSet *tptr)
{
   int tset, best;
   double normfac;


   for(tset=0; tset<tptr->ntrain ;tset++)
   {
      best = Winner(tptr->input[tset], &normfac);
      mapNeuron[best] = tset;
   }

} // CreateMap

////////////////////////////////////////
// public 
////////////////////////////////////////

int Kohonen::Winner(double *input, double *normfac)
{
   int i, win=0;
   double biggest;
   double *optr;


   NormalizeInput(input, normfac);

   biggest = -1.E30;
   for (i=0; i<NUMBER_OUTPUT; i++) 
   {
      optr = outputWeights[i];
      output[i] = dotProduct (NUMBER_INPUT, input, optr ) * (*normfac);
      
      output[i] = 0.5 * (output[i] + 1.0) ;
      if ( output[i] > biggest ) 
      {
         biggest = output[i] ;
         win = i ;
      }
      
      // account for rounding
      if ( output[i] > 1.0 )
         output[i] = 1.0;
      if ( output[i] < 0.0 )
         output[i] = 0.0;
   }

   return win ;
} /* Winner */


BOOL Kohonen::Learn(TrainingSet *tptr)
{
   int i, tset, iter, n_retry, nwts;
   int won[NUMBER_OUTPUT];
   int winners ;
   double **correc;
   double rate, best_err, *dptr;
   double bigerr;
   double bigcorr;
   Kohonen *bestnet;


   correc = new double* [NUMBER_OUTPUT];
   for(i=0; i<NUMBER_OUTPUT; i++)
   {
      correc[i] = new double[NUMBER_INPUT];
   }

   s_neterr = 1.0;
   for (tset=0; tset<tptr->ntrain; tset++) 
   {
      dptr = tptr->input[tset];
      if (VectorLength (NUMBER_INPUT, dptr) < 1.e-30) 
      {
         return FALSE;
      }
   }

   bestnet = new Kohonen() ;
   if (bestnet == NULL)
      return FALSE;

   nwts = NUMBER_INPUT * NUMBER_OUTPUT;
   rate = LEARN_RATE;

   Initialize();
   best_err = 1.e30;

   
   // gl�wna p�tla ucz�ca
   n_retry = 0;
   for (iter=0; ; iter++) 
   {

      EvaluateErrors(tptr, rate, won, &bigerr, correc);

      s_neterr = bigerr ;

      if (s_neterr < best_err) 
      {
         best_err = s_neterr ;
         CopyWeights(bestnet, this);
      }

      winners = 0;
      for (i=0; i<NUMBER_OUTPUT; i++ ) 
      {
         if (won[i] != 0)
            winners++;
      }

      if (bigerr < QUIT_ERROR)
         break ;

      if ((winners < NUMBER_OUTPUT)  &&  (winners < tptr->ntrain)) 
      {
         ForceWin(tptr, won);
         continue;
      }
      
      AdjustWeights(rate, won, &bigcorr, correc);


      if (bigcorr < 1.e-5) 
      {
         if (++n_retry > RETRIES)
            break;
         Initialize();
         iter = -1;
         rate = LEARN_RATE;
         continue ;
      }

      if (rate > 0.01)
         rate *= REDUCTION;
   }

   CopyWeights(this, bestnet);

   for (i=0; i<NUMBER_OUTPUT; i++)
      NormalizeWeight( outputWeights[i] );

   CreateMap(tptr);

   delete bestnet;
   for(i=0; i<NUMBER_OUTPUT; i++)
   {
      delete correc[i];
   }
   delete correc;

   return TRUE;
} /* Learn */

int* Kohonen::GetMapNeurons()
{
   return &(mapNeuron[0]);
}