#include <windows.h>
#include <stdio.h>
#include "resource.h"
#include "globals.h"
#include "Plansza.h"
#include "Kohonen.h"


static int samplesData[NUMBER_OF_LETTERS][DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT];
static double sampleOne[DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT];
static const int MAX_LINE = DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT+3;
static BOOL bTrain = FALSE;

BOOL CALLBACK DlgProc (HWND, UINT, WPARAM, LPARAM) ;


int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
                    PSTR szCmdLine, int iCmdShow)
{
   if (-1 == DialogBox (hInstance, MAKEINTRESOURCE(IDD_MAIN_DLG), NULL, DlgProc))
   {
       MessageBox (NULL, TEXT ("This program requires Windows NT!"),
                   MAKEINTRESOURCE(IDD_MAIN_DLG), MB_ICONERROR) ;
   }   
   return 0 ;
}


static void LoadPatern()
{
   FILE  *pFile;
   char  szLine[MAX_LINE];
   int   nIdx, nLine;
   char  szNumber[2];

   
   if( (pFile = fopen("data.txt", "rt" )) == NULL )
      return;

   nLine = 0;
   while(fgets(szLine, MAX_LINE+2, pFile) != NULL)
   {
      for(nIdx=0; nIdx<DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT; nIdx++)
      {      
         szNumber[0] = szLine[nIdx+3];
         szNumber[1] = '\0';
         samplesData[nLine][nIdx] = atoi(szNumber);
      }

      nLine++;
   }

   fclose(pFile);
}

static void SavePatern()
{
   FILE *pFile;
   char szText[MAX_LINE];
   char szNumber[2];
   int nIdx1, nIdx2;

   
   if( (pFile = fopen("data.txt", "wt" )) == NULL )
      return;

   for(nIdx1=0; nIdx1<NUMBER_OF_LETTERS; nIdx1++)
   {
      memset( szText, '\0', MAX_LINE);
      szText[0] = 'A' + nIdx1;
      strcat(szText, ": "); 
      for(nIdx2=0; nIdx2<DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT; nIdx2++)
      {
         sprintf(szNumber, "%d", samplesData[nIdx1][nIdx2]);
         strcat(szText, szNumber);
      }
      strcat(szText, "\n");
      fprintf (pFile, "%s", szText);   
   }
   
   fclose(pFile);
}

static void Train(Kohonen *pnet)
{
   int i, j;
   double val;
   TrainingSet tset;

   for(i=0; i<NUMBER_OF_LETTERS; i++)
   {
      for(j=0; j<DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT; j++)
      {
         if(samplesData[i][j] == 1)
            val = MAX_INPUT;
         else
            val = MIN_INPUT;

         tset.SetInput(i, j, val);
      }
   }   
   pnet->Learn(&tset);
   bTrain = TRUE;
   MessageBox(NULL, "Sie� zosta�a nauczona rozpoznawania liter.", 
               "Informacja", MB_OK | MB_ICONINFORMATION);

}

static void Recognize(HWND hWnd, Kohonen *pnet)
{
   
   double   norm;
   int      nNeuron;
   char     szText[200];
   char     szLetter[2];
   int      *pmap;
   
   if(!bTrain)
   {
      MessageBox(hWnd, "Sie� nie zosta�a jeszcze wytrenowana.\nNaci�nij przycisk 'Trenuj sie�'", 
                  "Informacja", MB_OK | MB_ICONWARNING);
      return;
   }
   Plansza_Rozpoznaj();
   Plansza_GetSample(sampleOne);
   nNeuron = pnet->Winner(sampleOne, &norm);
   pmap = pnet->GetMapNeurons();
   szLetter[0] = 'A' + pmap[nNeuron];
   szLetter[1] = '\0';
   sprintf(szText, "Rozpoznano liter�: %s", szLetter);
   SetDlgItemText(hWnd, IDC_INFO, szText); 
}


BOOL CALLBACK DlgProc (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
   RECT           rect ;
   static Kohonen *net = new Kohonen();   
   static HBITMAP hBitmap ;
   static int     cxBitmap, cyBitmap;
   static POINT   ptBitmap;
   HDC            hdc;
   static HDC     s_hdcMem;
   PAINTSTRUCT    ps;

   switch (message)
   {
      case WM_INITDIALOG:
         POINT pt;
         int width, height;
         int nIdx;
         char szString[2];
         BITMAP bufBitmap;

         
         Plansza_RegisterClass();

         // Wypo�rodkowanie okna dialogu
         GetWindowRect (hwnd, &rect);
         SetWindowPos (hwnd, NULL, 
            (GetSystemMetrics (SM_CXSCREEN) - rect.right + rect.left) / 2,
            (GetSystemMetrics (SM_CYSCREEN) - rect.bottom + rect.top) / 2,
            0, 0, SWP_NOZORDER | SWP_NOSIZE) ;

         GetWindowRect(GetDlgItem(hwnd, IDC_DRAW_WND), &rect);
        
         width = rect.right-rect.left;
         height = rect.bottom-rect.top;
         pt.x = rect.left;
         pt.y = rect.top;
         ScreenToClient(hwnd, &pt);
         
         CreateWindow("PLANSZA", NULL, WS_VISIBLE|WS_CHILD,
                     pt.x, pt.y, width, height,
                     hwnd, NULL, (HINSTANCE) GetModuleHandle(0), NULL);
         
         CheckRadioButton(hwnd, IDC_PATERN, IDC_RECOG, IDC_PATERN);
         EnableWindow(GetDlgItem(hwnd, IDC_TRAIN) , FALSE);
         EnableWindow(GetDlgItem(hwnd, IDC_RECOGNIZE) , FALSE);
         
         // wype�nienie listbox-u
         for(nIdx=0; nIdx<NUMBER_OF_LETTERS; nIdx++)
         {
            szString[0] = 'A' + nIdx;
            szString[1] = '\0';
            SendMessage (GetDlgItem(hwnd, IDC_LETTERS), LB_ADDSTRING, 0, (LPARAM) szString) ;
         }        
         SendMessage (GetDlgItem(hwnd, IDC_LETTERS), LB_SETCURSEL, 0, 0) ;
         
         // za�adowanie bitmapy
         hdc = GetDC(hwnd) ;
         hBitmap = LoadBitmap((HINSTANCE) GetModuleHandle(0), MAKEINTRESOURCE(IDB_SOFTWARE));
         s_hdcMem  = CreateCompatibleDC(hdc);
         ReleaseDC (hwnd, hdc) ;

         if (!hBitmap)       // brak pamieci dla bitmapy
         {
            DeleteDC (s_hdcMem) ;
            return TRUE;
         }
         GetObject(hBitmap, sizeof(BITMAP), &bufBitmap);
         cxBitmap = bufBitmap.bmWidth;
         cyBitmap = bufBitmap.bmHeight;
         GetWindowRect(GetDlgItem(hwnd, IDC_IMAGE), &rect);
         ptBitmap.x = rect.left;
         ptBitmap.y = rect.top;
         ScreenToClient(hwnd, &ptBitmap);
         SelectObject (s_hdcMem, hBitmap);         
         
         // za�adowanie wzorc�w z pliku data.txt        
         LoadPatern();
         
         SetFocus(GetDlgItem(hwnd, IDCANCEL));
         return FALSE ;
      case WM_COMMAND:
         switch (LOWORD (wParam))
         {
            case IDCANCEL:
               EndDialog (hwnd, 0) ;
               return TRUE;

            case IDC_PATERN:
               EnableWindow(GetDlgItem(hwnd, IDC_TRAIN) , FALSE);
               EnableWindow(GetDlgItem(hwnd, IDC_RECOGNIZE) , FALSE);               
               EnableWindow(GetDlgItem(hwnd, IDC_ADD_PATERN), TRUE);
               EnableWindow(GetDlgItem(hwnd, IDC_SAVE_PATERN), TRUE);
               EnableWindow(GetDlgItem(hwnd, IDC_LETTERS), TRUE);               
               break;

            case IDC_RECOG:
               EnableWindow(GetDlgItem(hwnd, IDC_TRAIN), TRUE);
               EnableWindow(GetDlgItem(hwnd, IDC_RECOGNIZE), TRUE);               
               EnableWindow(GetDlgItem(hwnd, IDC_ADD_PATERN), FALSE);
               EnableWindow(GetDlgItem(hwnd, IDC_SAVE_PATERN), FALSE);
               EnableWindow(GetDlgItem(hwnd, IDC_LETTERS), FALSE);               
               break;

            case IDC_TRAIN:
               Train(net);               
               break;

            case IDC_RECOGNIZE:
               Recognize(hwnd, net);
               break;

            case IDC_ADD_PATERN:
               int flag;
               int nIdx;
               int nLetter;

               Plansza_Rozpoznaj();
               Plansza_GetSample(sampleOne);
               nLetter = SendMessage (GetDlgItem(hwnd, IDC_LETTERS), LB_GETCURSEL, 0, 0) ;
               for(nIdx=0; nIdx<DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT; nIdx++)
               {
                  if(sampleOne[nIdx] == MIN_INPUT)
                     flag = 0;
                  else
                     flag = 1;
                  samplesData[nLetter][nIdx] = flag;
               }
               break;
            case IDC_SAVE_PATERN:
               SavePatern();
               break;
            case IDC_CLEAR:
               Plansza_Clear();
               SetDlgItemText(hwnd, IDC_INFO, "");
               break;
         }
         break;
      case WM_PAINT:
          hdc = BeginPaint (hwnd, &ps);
          BitBlt (hdc, ptBitmap.x, ptBitmap.y, cxBitmap, cyBitmap, s_hdcMem, 0, 0, SRCCOPY);
          EndPaint (hwnd, &ps);
          return 0 ;
      case WM_SYSCOMMAND:
         switch (LOWORD (wParam))
         {
         case SC_CLOSE:
            EndDialog (hwnd, 0) ;
            return TRUE ;
         }
         break ;
      case WM_DESTROY:
         DeleteDC (s_hdcMem);
         DeleteObject (hBitmap);
         PostQuitMessage (0);
         if(net)
            delete net;
         return 0; 
   }
   return FALSE ;
}
