
#ifndef _PLANSZA_
#define _PLANSZA_

#include <windows.h>

ATOM Plansza_RegisterClass();
LRESULT CALLBACK CtrlProc(HWND, UINT, WPARAM, LPARAM);

void Plansza_Rozpoznaj();
void Plansza_GetSample(double pSample[]);
void Plansza_Clear();

#endif