//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by pismo.rc
//
#define IDD_MAIN_DLG                    101
#define IDB_SOFTWARE                    103
#define IDC_DRAW_WND                    1000
#define IDC_INFO                        1001
#define IDC_TRAIN                       1003
#define IDC_SAVE_PATERN                 1004
#define IDC_ADD_PATERN                  1005
#define IDC_RECOGNIZE                   1006
#define IDC_LETTERS                     1007
#define IDC_CLEAR                       1008
#define IDC_IMAGE                       1009
#define IDC_PATERN                      1010
#define IDC_RECOG                       1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
