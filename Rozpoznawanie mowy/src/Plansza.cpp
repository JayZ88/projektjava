/*--------------------------------------------------
   PLANSZA.CPP -- Kontrolka, w kt�rej b�dzie mo�na
                  narysowa� liter�

   Miesi�cznik "Software 2.0" - luty 2003.

   Autor: Krzysztof Osipowicz
   www:    www.krzyso.prv.pl
   e-mail: krzyso@poczta.onet.pl
  -------------------------------------------------*/

#include "globals.h"
#include "Plansza.h"

static int  cxClient, cyClient;
static HWND s_hWnd;
static HDC  s_hdcMem ;
static double sample[DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT];

ATOM Plansza_RegisterClass()
{
	WNDCLASS wclass;

   wclass.style = CS_HREDRAW|CS_VREDRAW;
	wclass.lpfnWndProc = (WNDPROC)CtrlProc;
	wclass.cbClsExtra = 0;
	wclass.cbWndExtra = 0;
	wclass.hInstance = (HINSTANCE) GetModuleHandle(0);
	wclass.hIcon = NULL;
	wclass.hCursor = NULL;
	
	wclass.hbrBackground = (HBRUSH)(GetStockObject(WHITE_BRUSH));
	wclass.lpszClassName = "PLANSZA";
	wclass.lpszMenuName = NULL;


	return RegisterClass(&wclass);
}

void GetLargestDisplayMode (int *pcxBitmap, int *pcyBitmap)
{
     DEVMODE devmode ;
     int     iModeNum = 0 ;

     * pcxBitmap = * pcyBitmap = 0 ;

     ZeroMemory (&devmode, sizeof (DEVMODE)) ;
     devmode.dmSize = sizeof (DEVMODE) ;
     
     while (EnumDisplaySettings (NULL, iModeNum++, &devmode))
     {
          * pcxBitmap = max (* pcxBitmap, (int) devmode.dmPelsWidth) ;
          * pcyBitmap = max (* pcyBitmap, (int) devmode.dmPelsHeight) ;
     }
}

LRESULT CALLBACK CtrlProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
   static BOOL    fLeftButtonDown;
   static HBITMAP hBitmap;
   static int     cxBitmap, cyBitmap, xMouse, yMouse;
   HDC            hdc;
   PAINTSTRUCT    ps;
	
   
   s_hWnd = hwnd;
   switch (message) 
	{
      case WM_CREATE:
          GetLargestDisplayMode (&cxBitmap, &cyBitmap) ;

          hdc = GetDC (hwnd) ;
          hBitmap = CreateCompatibleBitmap (hdc, cxBitmap, cyBitmap) ;
          s_hdcMem  = CreateCompatibleDC (hdc) ;
          ReleaseDC (hwnd, hdc) ;

          if (!hBitmap)       // brak pamieci dla bitmapy
          {
               DeleteDC (s_hdcMem) ;
               return -1 ;
          }

          SelectObject (s_hdcMem, hBitmap) ;
          PatBlt (s_hdcMem, 0, 0, cxBitmap, cyBitmap, WHITENESS) ;
          return 0 ;

      case WM_SIZE:
          cxClient = LOWORD (lParam) ;
          cyClient = HIWORD (lParam) ;
          return 0 ;

      case WM_LBUTTONDOWN:
          SetCapture (hwnd) ;
          xMouse = LOWORD (lParam) ;
          yMouse = HIWORD (lParam) ;
          fLeftButtonDown = TRUE ;
          return 0 ;

      case WM_LBUTTONUP:
          if (fLeftButtonDown)
               SetCapture (NULL) ;
    
          fLeftButtonDown = FALSE ;
          return 0 ;
    
      case WM_MOUSEMOVE:
          if (!fLeftButtonDown)
              return 0 ;

          hdc = GetDC (hwnd);

          SelectObject (hdc, GetStockObject (BLACK_PEN));

          SelectObject (s_hdcMem, GetStockObject (BLACK_PEN));

          MoveToEx (hdc,    xMouse, yMouse, NULL);
          MoveToEx (s_hdcMem, xMouse, yMouse, NULL);

          xMouse = (short) LOWORD (lParam);
          yMouse = (short) HIWORD (lParam);

          LineTo (hdc,    xMouse, yMouse);
          LineTo (s_hdcMem, xMouse, yMouse);

          ReleaseDC (hwnd, hdc);
          return 0 ;

      case WM_PAINT:
          hdc = BeginPaint (hwnd, &ps);

          BitBlt (hdc, 0, 0, cxClient, cyClient, s_hdcMem, 0, 0, SRCCOPY) ;

          EndPaint (hwnd, &ps) ;
          return 0 ;

      case WM_DESTROY:
          DeleteDC (s_hdcMem) ;
          DeleteObject (hBitmap) ;
          PostQuitMessage (0) ;
          return 0 ;   
   }
   return DefWindowProc (hwnd, message, wParam, lParam) ;
}

BOOL IsHLineClear(HDC hdc, int line)
{
   int j;

   for(j=0; j<cxClient; j++)
   {
      if(GetPixel(hdc, j, line) == 0)
      {
         return FALSE;
      }      
   }
   return TRUE;
}

BOOL IsVLineClear(HDC hdc, int line)
{
   int j;

   for(j=0; j<cyClient; j++)
   {
      if(GetPixel(hdc, line, j) == 0)
      { 
         return FALSE;
      }
   }
   return TRUE;
}

BOOL IsRectClear(HDC hdc, RECT rect)
{
   int i, j;

   for(i=rect.left; i<rect.right; i++)
   {
      for(j=rect.top; j<rect.bottom; j++)
      {
         if(GetPixel(hdc, i, j) == 0)
         {
            return FALSE;
         }
      }
   }
   return TRUE;
}

void Plansza_Rozpoznaj()
{
   int i;
   HDC hdc;
   int downSampleLeft;
   int downSampleRight;
   int downSampleTop;
   int downSampleBottom;
   RECT rect;
   int widthRect;
   int heightRect;
   int j, idx;
   HPEN hPen;


   hdc = GetDC(s_hWnd);  

   for(i=0; i<cxClient; i++)
   {
      if(!IsVLineClear(hdc, i))
      {
         downSampleLeft = i; 
         break;
      }
   }

   for(i=cxClient; i>0; i--)
   {
      if(!IsVLineClear(hdc, i))
      {
         downSampleRight = i; 
         break;
      }
   }

   for(i=0; i<cyClient; i++)
   {
      if(!IsHLineClear(hdc, i))
      {
         downSampleTop = i; 
         break;
      }
   }

   for(i=cxClient; i>0; i--)
   {
      if(!IsHLineClear(hdc, i))
      {
         downSampleBottom = i; 
         break;
      }
   }

   ///// probkowanie
   widthRect = (downSampleRight - downSampleLeft)/DOWNSAMPLE_WIDTH;
   heightRect = (downSampleBottom - downSampleTop)/DOWNSAMPLE_HEIGHT;
   idx=0;
   for(i=0; i<DOWNSAMPLE_HEIGHT; i++)
   {
      for(j=0; j<DOWNSAMPLE_WIDTH; j++)
      {
         rect.left  = downSampleLeft + j*widthRect;
         rect.right = rect.left + widthRect;
         rect.top   = downSampleTop + i*heightRect;
         rect.bottom = rect.top + heightRect;
         if(IsRectClear(hdc, rect))
         {
            sample[idx++] = MIN_INPUT;
         }
         else
         {
            sample[idx++] = MAX_INPUT;
         }
      }
   }      
   ///// koniec probkowania ////
   
   // rysowanie ramki wok� litery
   hPen = CreatePen(PS_SOLID, 0, RGB(255, 0, 0) );

   SelectObject (hdc, hPen);
   SelectObject (s_hdcMem, hPen);
   
   MoveToEx (hdc, downSampleLeft, downSampleTop, NULL);
   MoveToEx (s_hdcMem, downSampleLeft, downSampleTop, NULL);

   LineTo (hdc, downSampleRight, downSampleTop) ;
   LineTo (s_hdcMem, downSampleRight, downSampleTop);

   LineTo (hdc, downSampleRight, downSampleBottom) ;
   LineTo (s_hdcMem, downSampleRight, downSampleBottom);

   LineTo (hdc, downSampleLeft, downSampleBottom) ;
   LineTo (s_hdcMem, downSampleLeft, downSampleBottom);

   LineTo (hdc, downSampleLeft, downSampleTop) ;
   LineTo (s_hdcMem, downSampleLeft, downSampleTop);
   
   InvalidateRect(s_hWnd, NULL, TRUE);
   UpdateWindow(s_hWnd);

   DeleteObject(hPen);

   ReleaseDC (s_hWnd, hdc);
}

void Plansza_GetSample(double pSample[])
{
   int nIdx;

   for(nIdx=0; nIdx<DOWNSAMPLE_WIDTH*DOWNSAMPLE_HEIGHT; nIdx++)
   {
      pSample[nIdx] = sample[nIdx];
   }
}

void Plansza_Clear()
{
   HDC hdc;
   RECT rect;

   GetClientRect(s_hWnd, &rect);
   hdc = GetDC(s_hWnd);
   FillRect(hdc, &rect, (HBRUSH)(GetStockObject(WHITE_BRUSH)) );
   FillRect(s_hdcMem, &rect, (HBRUSH)(GetStockObject(WHITE_BRUSH)) );
   ReleaseDC (s_hWnd, hdc);

   InvalidateRect(s_hWnd, NULL, TRUE);
   UpdateWindow(s_hWnd);
}
