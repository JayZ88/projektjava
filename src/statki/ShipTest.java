/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statki;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Jay Z
 */
public class ShipTest extends javax.swing.JFrame
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        Random rand = new Random();
        Ship ship = new Ship();
        String userGuess;
        int numOfGuess = 0;
        boolean isAlive = true;
        int temp = rand.nextInt(5)+1;
        ArrayList<String> locations = new ArrayList<String>();
        ship.setLocations(locations);
        
        while (isAlive)
        {
            String result;
            System.out.println("Enter a guess");
            userGuess = input.nextLine();
            result = ship.checkGuess(userGuess);
            numOfGuess++;
            
            if (result.equals("kill"))
            {
                isAlive = false;
            }
            
        }
        
        System.out.println("You took" + numOfGuess + "guesses to sink a ship");
        
    }
}
