/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statki;

import java.util.ArrayList;

/**
 *
 * @author Jay Z
 */
public class Ship {
    
    private ArrayList<String> location = new ArrayList<String>();
    private int numOfHits = 0;
    
    public void setLocations(ArrayList<String> loc)
    {
        location = loc;
    }
    
    public String checkGuess(String Userguess)
    {
        String result = "miss";
        int index = location.indexOf(Userguess);
        
        if (index >= 0)
        {
            location.remove(index);
            result = location.isEmpty() ? "kill" : "hit";
        }
        
        System.out.println(result);
        
        return result;
    }
}
